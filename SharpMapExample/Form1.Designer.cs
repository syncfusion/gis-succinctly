﻿namespace SharpMapExample1
{
  partial class formMainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.mpbMapView = new SharpMap.Forms.MapBox();
      this.btnZoomAndPan = new System.Windows.Forms.Button();
      this.btnQueryCounty = new System.Windows.Forms.Button();
      this.lsbCountyResults = new System.Windows.Forms.ListBox();
      this.StatusBar = new System.Windows.Forms.StatusStrip();
      this.lblStatusText = new System.Windows.Forms.ToolStripStatusLabel();
      this.StatusBar.SuspendLayout();
      this.SuspendLayout();
      // 
      // mpbMapView
      // 
      this.mpbMapView.ActiveTool = SharpMap.Forms.MapBox.Tools.None;
      this.mpbMapView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.mpbMapView.BackColor = System.Drawing.Color.White;
      this.mpbMapView.Cursor = System.Windows.Forms.Cursors.Default;
      this.mpbMapView.FineZoomFactor = 10D;
      this.mpbMapView.Location = new System.Drawing.Point(189, 12);
      this.mpbMapView.Name = "mpbMapView";
      this.mpbMapView.QueryGrowFactor = 5F;
      this.mpbMapView.QueryLayerIndex = 0;
      this.mpbMapView.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
      this.mpbMapView.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
      this.mpbMapView.ShowProgressUpdate = false;
      this.mpbMapView.Size = new System.Drawing.Size(783, 648);
      this.mpbMapView.TabIndex = 0;
      this.mpbMapView.Text = "mapBox1";
      this.mpbMapView.WheelZoomMagnitude = -2D;
      this.mpbMapView.MapRefreshed += new System.EventHandler(this.MpbMapViewMapRefreshed);
      this.mpbMapView.Click += new System.EventHandler(this.MpbMapViewClick);
      // 
      // btnZoomAndPan
      // 
      this.btnZoomAndPan.Location = new System.Drawing.Point(12, 12);
      this.btnZoomAndPan.Name = "btnZoomAndPan";
      this.btnZoomAndPan.Size = new System.Drawing.Size(171, 23);
      this.btnZoomAndPan.TabIndex = 1;
      this.btnZoomAndPan.Text = "Switch to Zoom && Pan Mode";
      this.btnZoomAndPan.UseVisualStyleBackColor = true;
      this.btnZoomAndPan.Click += new System.EventHandler(this.BtnZoomAndPanClick);
      // 
      // btnQueryCounty
      // 
      this.btnQueryCounty.Location = new System.Drawing.Point(13, 42);
      this.btnQueryCounty.Name = "btnQueryCounty";
      this.btnQueryCounty.Size = new System.Drawing.Size(170, 23);
      this.btnQueryCounty.TabIndex = 2;
      this.btnQueryCounty.Text = "Switch to County Query Mode";
      this.btnQueryCounty.UseVisualStyleBackColor = true;
      this.btnQueryCounty.Click += new System.EventHandler(this.BtnQueryCountyClick);
      // 
      // lsbCountyResults
      // 
      this.lsbCountyResults.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
      this.lsbCountyResults.FormattingEnabled = true;
      this.lsbCountyResults.Location = new System.Drawing.Point(13, 71);
      this.lsbCountyResults.Name = "lsbCountyResults";
      this.lsbCountyResults.Size = new System.Drawing.Size(170, 589);
      this.lsbCountyResults.TabIndex = 3;
      // 
      // StatusBar
      // 
      this.StatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusText});
      this.StatusBar.Location = new System.Drawing.Point(0, 681);
      this.StatusBar.Name = "StatusBar";
      this.StatusBar.Size = new System.Drawing.Size(984, 22);
      this.StatusBar.TabIndex = 4;
      this.StatusBar.Text = "statusStrip1";
      // 
      // lblStatusText
      // 
      this.lblStatusText.Name = "lblStatusText";
      this.lblStatusText.Size = new System.Drawing.Size(0, 17);
      // 
      // formMainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(984, 703);
      this.Controls.Add(this.StatusBar);
      this.Controls.Add(this.lsbCountyResults);
      this.Controls.Add(this.btnQueryCounty);
      this.Controls.Add(this.btnZoomAndPan);
      this.Controls.Add(this.mpbMapView);
      this.Name = "formMainForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Sharp Map Example 1";
      this.StatusBar.ResumeLayout(false);
      this.StatusBar.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private SharpMap.Forms.MapBox mpbMapView;
    private System.Windows.Forms.Button btnZoomAndPan;
    private System.Windows.Forms.Button btnQueryCounty;
    private System.Windows.Forms.ListBox lsbCountyResults;
    private System.Windows.Forms.StatusStrip StatusBar;
    private System.Windows.Forms.ToolStripStatusLabel lblStatusText;
  }
}


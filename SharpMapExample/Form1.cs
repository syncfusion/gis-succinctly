﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using GeoAPI.Geometries;
using Npgsql;
using NpgsqlTypes;
using SharpMap.Data;
using SharpMap.Data.Providers;
using SharpMap.Forms;
using SharpMap.Layers;
using SharpMap.Styles;

namespace SharpMapExample1
{
  public partial class formMainForm : Form
  {
    private const string _connString = "Server=vbox01;Port=5432;User Id=gisuser;Password=gisuser;Database=gisbook;CommandTimeout=300";
    private bool _mapInitializing;

    public formMainForm()
    {
      InitializeComponent();
      InitializeMap();
    }

    public void InitializeMap()
    {
      _mapInitializing = true;
      lblStatusText.Text = "MAP Initializing please wait";

      const string countyTableName = "ukmap";
      const string countyGeometryColumnName = "the_geom";
      const string countyGidColumnName = "gid";

      const string cityTableName = "ukcitys_wgs";
      const string cityGeometryColumnName = "geometry";
      const string cityGidColumnName = "gid";

      const string townTableName = "uktowns_wgs";
      const string townGeometryColumnName = "geometry";
      const string townGidColumnName = "gid";

      VectorStyle ukCountyStyle = new VectorStyle { Fill = Brushes.Green, Outline = Pens.Black, EnableOutline = true };
      VectorLayer ukCountys = new VectorLayer("ukmaps")
      {
          Style = ukCountyStyle,
          DataSource = new PostGIS(_connString, countyTableName, countyGeometryColumnName, countyGidColumnName),
          MaxVisible = 40000
      };

      VectorStyle ukCityStyle = new VectorStyle { PointColor = Brushes.OrangeRed };
      VectorLayer ukCitys = new VectorLayer("ukcitys")
      {
        Style = ukCityStyle,
        DataSource = new PostGIS(_connString, cityTableName, cityGeometryColumnName, cityGidColumnName),
        MaxVisible = 40000,
      };

      VectorStyle ukTownStyle = new VectorStyle { PointColor = Brushes.DodgerBlue };
      VectorLayer ukTowns = new VectorLayer("uktowns")
      {
        Style = ukTownStyle,
        DataSource = new PostGIS(_connString, townTableName, townGeometryColumnName, townGidColumnName),
        MaxVisible = 40000,
      };

      mpbMapView.ActiveTool = MapBox.Tools.Pan;
      mpbMapView.Map.Layers.Add(ukCountys);
      mpbMapView.Map.Layers.Add(ukCitys);
      mpbMapView.Map.Layers.Add(ukTowns);
      mpbMapView.Map.ZoomToExtents();
      mpbMapView.Refresh();
      
    }

    private void MpbMapViewMapRefreshed(object sender, System.EventArgs e)
    {
      if(_mapInitializing)
      {
        _mapInitializing = false;
        lblStatusText.Text = "MAP Initialized";
      }
    }

    private void BtnZoomAndPanClick(object sender, System.EventArgs e)
    {
      mpbMapView.ActiveTool = MapBox.Tools.Pan;
    }

    private void BtnQueryCountyClick(object sender, System.EventArgs e)
    {
      mpbMapView.ActiveTool = MapBox.Tools.Query;
    }

    private void MpbMapViewClick(object sender, System.EventArgs e)
    {
      if (mpbMapView.ActiveTool != MapBox.Tools.Query) return;

      lblStatusText.Text = "Querying Map... please wait for results.";
      Application.DoEvents();

      MouseEventArgs myMouse = (MouseEventArgs)e;
      Coordinate wgs84Location = mpbMapView.Map.ImageToWorld(new PointF(myMouse.X, myMouse.Y));

      FeatureDataSet selectedGeometry = new FeatureDataSet();

      VectorLayer theLayer = (VectorLayer)mpbMapView.Map.FindLayer("ukcountys").FirstOrDefault();
      if (theLayer != null)
      {
        if (!theLayer.DataSource.IsOpen)
        {
          theLayer.DataSource.Open();
        }

        Envelope boundingBox = new Envelope(wgs84Location.CoordinateValue);
        if (Math.Abs(boundingBox.Area - 0.0) < 0.01)
        {
          boundingBox.ExpandBy(0.01);
        }

        theLayer.DataSource.ExecuteIntersectionQuery(boundingBox, selectedGeometry);
        theLayer.DataSource.Close();
      }

      if (selectedGeometry.Tables[0].Count <= 0) return;
      string countyName = selectedGeometry.Tables[0].Rows[0]["name2"].ToString();

      List<string> cityList = GetCitysForCounty(countyName);
      List<string> townList = GetTownsForCounty(countyName);

      VectorLayer highlightLayer = (VectorLayer)mpbMapView.Map.FindLayer("highlight").FirstOrDefault();
      if (highlightLayer == null)
      {
        Color myColor = Color.FromArgb(64, 144, 238, 144);
        Brush fillBrush = new SolidBrush(myColor);

        highlightLayer = new VectorLayer("highlight");
        VectorStyle highlightStyle = new VectorStyle { Fill = fillBrush, Outline = Pens.White, EnableOutline = true };
        highlightLayer.Style = highlightStyle;
      }

      highlightLayer.DataSource = new GeometryProvider(selectedGeometry.Tables[0]);
      mpbMapView.Map.Layers.Add(highlightLayer);
      mpbMapView.Refresh();

      lsbCountyResults.Items.Clear();
      lsbCountyResults.Items.Add("Selected county: " + countyName.ToUpper());
      lsbCountyResults.Items.Add("");

      if (cityList.Count > 0)
      {
        lsbCountyResults.Items.Add("Citys in this county");
        foreach (string city in cityList)
        {
          lsbCountyResults.Items.Add(city);
        }
        lsbCountyResults.Items.Add("");
      }

      if (townList.Count > 0)
      {
        lsbCountyResults.Items.Add("Towns in this county");
        foreach (string town in townList)
        {
          lsbCountyResults.Items.Add(town);
        }
        lsbCountyResults.Items.Add("");
      }

      lblStatusText.Text = "Query finished.";
    }

    private static List<string> GetTownsForCounty(string countyName)
    {
      string sql =
        string.Format(
          "SELECT t.Name FROM ukcountys c, uktowns t WHERE name2 = :county AND ST_Within(t.geometry,ST_Transform(c.the_geom,27700))");
      List<string> results = new List<string>();

      using (NpgsqlConnection conn = new NpgsqlConnection(_connString))
      {
        conn.Open();
        using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
        {
          command.Parameters.Add(new NpgsqlParameter("county", NpgsqlDbType.Varchar));
          command.Parameters[0].Value = countyName;

          using (NpgsqlDataReader dr = command.ExecuteReader())
          {
            while (dr.Read())
            {
              results.Add(dr.GetString(0));
            }
          }
        }
      }

      return results;
    }

    private static List<string> GetCitysForCounty(string countyName)
    {
      string sql =
        string.Format(
          "SELECT t.Name FROM ukcountys c, ukcitys t WHERE name2 = :county AND ST_Within(t.geometry,ST_Transform(c.the_geom,27700))");
      List<string> results = new List<string>();

      using (NpgsqlConnection conn = new NpgsqlConnection(_connString))
      {
        conn.Open();
        using (NpgsqlCommand command = new NpgsqlCommand(sql, conn))
        {
          command.Parameters.Add(new NpgsqlParameter("county", NpgsqlDbType.Varchar));
          command.Parameters[0].Value = countyName;

          using (NpgsqlDataReader dr = command.ExecuteReader())
          {
            while (dr.Read())
            {
              results.Add(dr.GetString(0));
            }
          }
        }
      }

      return results;
    }

  }
}